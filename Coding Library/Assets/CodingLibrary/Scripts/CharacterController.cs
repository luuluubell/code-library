using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem; 


public class CharacterController : MonoBehaviour {
    Material Yeet; 
    MeshRenderer _meshrenderer; 
    void Awake(){
        Yeet = _meshrenderer.material; 
    }
    public void Move(InputAction.CallbackContext ctx){
        Vector2 moveVector = ctx.ReadValue<Vector2> (); 
        transform.Translate (moveVector.x, 0, moveVector.y); 
        ChangeColour (); 
    }
    
    public void ChangeColour (){
        Yeet.SetColor ("_BaseColor",  new Color(Random.Range(0,1f), Random.Range(0,1f),Random.Range(0,1f))); 
        _meshrenderer.material = Yeet; 
        Debug.Log("le color");
    }
    
}
